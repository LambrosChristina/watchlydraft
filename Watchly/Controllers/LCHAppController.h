//
//  LCHLoginViewController.h
//  Watchly
//
//  Created by Lambros Tabourlos on 6/5/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "LCHViewController.h"

@interface LCHAppController : LCHViewController

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *passwordVerificationField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *countryField;
@property (weak, nonatomic) IBOutlet UITextField *cityField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSegmentedControl;
@property (strong, nonatomic) IBOutlet UIScrollView *signUpView;


@property (strong, nonatomic) PFUser *user;

- (IBAction)login:(id)sender;
- (IBAction)signup:(id)sender;
- (IBAction)backgroundTap:(id)sender;

@end
