//
//  LCHViewController.h
//  Watchly
//
//  Created by Lambros Tabourlos on 6/18/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

extern PFUser *user;

@interface LCHViewController : UIViewController

@property (weak, nonatomic) UIColor *tintColor;

- (IBAction)backgroundTap:(id)sender;
- (IBAction)textFieldDoneEditing:(id)sender;

@end
