//
//  LCHUserProfileViewController.h
//  Watchly
//
//  Created by Lambros Tabourlos on 5/18/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LCHViewController.h"

@interface LCHUserProfileViewController : LCHViewController

@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *watchedLabel;
@property (weak, nonatomic) IBOutlet UILabel *friendsLabel;
@property (weak, nonatomic) IBOutlet UILabel *listsLabel;


@end
