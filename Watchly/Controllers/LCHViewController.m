//
//  LCHViewController.m
//  Watchly
//
//  Created by Lambros Tabourlos on 6/18/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import "LCHViewController.h"

@interface LCHViewController ()

@end

@implementation LCHViewController

PFUser *user;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.tintColor= [UIColor colorWithRed:22/255.0 green:136/255.0 blue:196/255.0 alpha:1.0];
    [self.navigationController.navigationBar setTintColor: self.tintColor];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backgroundTap:(id)sender {
}
- (IBAction)textFieldDoneEditing:(id)sender {
    [sender resignFirstResponder];
}

@end
