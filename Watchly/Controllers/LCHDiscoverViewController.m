//
//  LCHDiscoverViewController.m
//  Watchly
//
//  Created by Lambros Tabourlos on 7/9/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import "LCHDiscoverViewController.h"

@implementation UIImage (PhoenixMaster)
- (UIImage *) makeThumbnailOfSize:(CGSize)size;
{
    UIGraphicsBeginImageContext(size);
    // draw scaled image into thumbnail context
    [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newThumbnail = UIGraphicsGetImageFromCurrentImageContext();
    // pop the context
    UIGraphicsEndImageContext();
    if(newThumbnail == nil)
        NSLog(@"could not scale image");
    return newThumbnail;
}

@end


@interface LCHDiscoverViewController ()

@end

NSString *const url = @"http://cf2.imgobject.com/t/p/w500";

@implementation LCHDiscoverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.movies = [[LCHImdbMovieCollectionModel alloc] init];
    [self.movies getTopRatedMovies];
    [self.discoverTableView reloadData];
    
    self.discoverTableView.scrollEnabled = YES;
    
    self.searchButton.backgroundColor = [UIColor blackColor];
    self.makemydayButton.backgroundColor = [UIColor colorWithRed:(100/255.0) green:(100/255.0) blue:(100/255.0) alpha:1];
    
    self.searchField.backgroundColor = [UIColor colorWithRed:(230/255.0) green:(230/255.0) blue:(220/255.0) alpha:1];
    self.dropdownButton.backgroundColor = [UIColor colorWithRed:(230/255.0) green:(230/255.0) blue:(220/255.0) alpha:1];
    /*self.dropdownMenu = [[UIDropDownMenu alloc] initWithIdentifier:@"dropdown"];
    NSMutableArray *arrayNames = [[NSMutableArray alloc] initWithObjects:
                                  @"Erik Vanderwal",
                                  @"Max Town",
                                  @"Darryl Totman",
                                  @"Avis Villalon",
                                  @"Hugh Salvia",
                                  @"Allie Maland",
                                  nil];
    
    self.dropdownMenu.ScaleToFitParent = TRUE;
    self.dropdownMenu.titleArray = arrayNames;
    self.dropdownMenu.valueArray = arrayNames;
    [self.dropdownMenu makeMenu:self.dropdownButton targetView:self.view];
    self.dropdownMenu.delegate = self;*/
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)backgroundTap:(id)sender {
    [self.searchField resignFirstResponder];
}

- (IBAction)showDropdown:(id)sender {
    
}

- (IBAction)toggleSearch:(id)sender {
    self.searchButton.selected = YES;
    self.makemydayButton.selected = NO;
    
    self.searchButton.backgroundColor = [UIColor blackColor];
    self.makemydayButton.backgroundColor = [UIColor colorWithRed:(100/255.0) green:(100/255.0) blue:(100/255.0) alpha:1];
    
    self.searchField.placeholder = @"Search Wathcly for films and TV series";
    [self.discoverTableView reloadData];
    
}

- (IBAction)toggleMakemyday:(id)sender {
    self.searchButton.selected = NO;
    self.makemydayButton.selected = YES;
    
    self.searchButton.backgroundColor = [UIColor colorWithRed:(100/255.0) green:(100/255.0) blue:(100/255.0) alpha:1];
    self.makemydayButton.backgroundColor = [UIColor blackColor];
    
    self.friends = [ user objectForKey:@"friends"];
    
    self.searchField.placeholder = @"Ask your friends for recommendations";
    
    [self.discoverTableView reloadData];
}

- (IBAction)textFieldDoneEditing:(id)sender {
    [self searchMovies:sender];
    [sender resignFirstResponder];
}

- (IBAction)searchMovies:(id)sender {
    if(self.searchField.text.length > 0) {
        [self.movies searchTheMovieDbApiByTitle:self.searchField.text];
    }
    [self.discoverTableView reloadData];
}

#pragma mark -
#pragma mark Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.makemydayButton.selected == NO) {
        return [self.movies.movieCollection count];
    }
    else {
        return [self.friends count];
    }
        
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    if(self.makemydayButton.selected == YES) {
        UIFont *labelFont = [ UIFont fontWithName:@"Helvetica-Bold" size: 15.0 ];
        UIFont *detailedLabelFont = [ UIFont fontWithName:@"Helvetica" size: 12.0 ];
        
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font  = labelFont;
        cell.detailTextLabel.font = detailedLabelFont;
        cell.detailTextLabel.textColor = [UIColor colorWithRed:22/255.0 green:136/255.0 blue:196/255.0 alpha:1.0];
        
        cell.backgroundColor = [UIColor colorWithRed:(245/255.0) green:(245/255.0) blue:(245/255.0) alpha:1];
    
        
        PFUser *friend = [self.friends objectAtIndex:indexPath.row];
        [friend fetch];
        cell.detailTextLabel.text = [friend objectForKey:@"username"];
        cell.textLabel.text = [friend objectForKey:@"username"];
    }
    else {
        
        UIFont *labelFont = [ UIFont fontWithName:@"Helvetica-Bold" size: 15.0 ];
        UIFont *detailedLabelFont = [ UIFont fontWithName:@"Helvetica" size: 12.0 ];
        
        cell.textLabel.textColor = [UIColor colorWithRed:22/255.0 green:136/255.0 blue:196/255.0 alpha:1.0];
        cell.textLabel.font  = labelFont;
        cell.detailTextLabel.font = detailedLabelFont;
        cell.backgroundColor = [UIColor colorWithRed:(245/255.0) green:(245/255.0) blue:(245/255.0) alpha:1];
    
        
        LCHImdbMovieModel *movie = [self.movies.movieCollection objectAtIndex:indexPath.row];
        
        NSString *path = [NSString stringWithFormat:@"%@%@", url, [movie poster]];
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:path]];
        UIImage *bigImage = [UIImage imageWithData:imageData];
        UIImage *thumb = [bigImage makeThumbnailOfSize:CGSizeMake(50,70)];
        
        cell.textLabel.text = [movie title];
        cell.detailTextLabel.text = (NSString *)[movie year];
        cell.imageView.image = thumb;
    }
    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [cell.imageView setFrame:CGRectMake(0, 0, 55, 55)];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}
@end
