//
//  LCHUserProfileViewController.m
//  Watchly
//
//  Created by Lambros Tabourlos on 5/18/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import "LCHUserProfileViewController.h"

@interface LCHUserProfileViewController ()

@end

@implementation LCHUserProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
