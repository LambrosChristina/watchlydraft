//
//  LCHDiscoverViewController.h
//  Watchly
//
//  Created by Lambros Tabourlos on 7/9/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LCHViewController.h"
#import "LCHImdbMovieCollectionModel.h"
#import "UIDropDownMenu.h"

@interface UIImage (PhoenixMaster)
- (UIImage *) makeThumbnailOfSize:(CGSize)size;
@end

@interface LCHDiscoverViewController : LCHViewController
    <UISearchBarDelegate, UITableViewDataSource>

@property (strong, nonatomic) UIDropDownMenu *dropdownMenu;

@property (strong, nonatomic) LCHImdbMovieCollectionModel *movies;
@property (strong , nonatomic) NSMutableArray* friends;

@property (weak, nonatomic) IBOutlet UITableView *discoverTableView;
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIButton *dropdownButton;
@property (weak, nonatomic) IBOutlet UIButton *makemydayButton;


- (IBAction)showDropdown:(id)sender;
- (IBAction)toggleSearch:(id)sender;
- (IBAction)toggleMakemyday:(id)sender;

- (IBAction)textFieldDoneEditing:(id)sender;
- (IBAction)backgroundTap:(id)sender;
- (IBAction)searchMovies:(id)sender;

@end
