//
//  LCHLoginViewController.m
//  Watchly
//
//  Created by Lambros Tabourlos on 6/5/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import "LCHAppController.h"
#import "LCHFriendModel.h"

@interface LCHAppController ()

@end

@implementation LCHAppController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.userNameLabel setTextColor: self.tintColor];
    [self.passwordLabel setTextColor: self.tintColor];

    [self.signUpView setScrollEnabled:YES];
    [self.signUpView setContentSize:CGSizeMake(300, 800)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backgroundTap:(id)sender {
    [self.usernameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
}
- (IBAction)login:(id)sender
{
    user = [PFUser user];
    
    if(self.usernameField.text.length == 0 ||
       self.passwordField.text.length == 0) {
        return;
    }
    user = [PFUser logInWithUsername:self.usernameField.text password:self.passwordField.text];
    if (user == nil) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"login failure"
                              message:@"Unknown user"
                              delegate:nil
                              cancelButtonTitle:@"Proceed"
                              otherButtonTitles:nil
                              ];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"login success"
                              message:@"Welcome!"
                              delegate:nil
                              cancelButtonTitle:@"Proceed"
                              otherButtonTitles:nil
                              ];
        [alert show];
        [self performSegueWithIdentifier:@"Login" sender:sender];
    }
}

- (IBAction)signup:(id)sender
{
    user = [PFUser user];
    
    if(self.usernameField.text.length == 0 ||
       self.passwordField.text.length == 0) {
        return;
    }
    
    user.username = self.usernameField.text;
    user.password = self.passwordField.text;
    user.email = self.emailField.text;
    [user setObject: self.cityField.text forKey:@"city"];
    [user setObject: self.countryField.text forKey:@"country"];
    [user setObject: [NSNumber numberWithInt: self.genderSegmentedControl.selectedSegmentIndex] forKey:@"gender"];

    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"signup success"
                                  message:@"You have successfully signed up"
                                  delegate:nil
                                  cancelButtonTitle:@"Proceed"
                                  otherButtonTitles:nil
                                  ];
            user = [PFUser logInWithUsername:self.usernameField.text password:self.passwordField.text];
            [alert show];
            [self performSegueWithIdentifier:@"SignUp" sender:sender];
        } else {
            NSString *errorString = [[error userInfo] objectForKey:@"error"];
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"signup failure"
                                  message:errorString
                                  delegate:nil
                                  cancelButtonTitle:@"Proceed"
                                  otherButtonTitles:nil
                                  ];
            [alert show];
        }
    }];
}

@end
