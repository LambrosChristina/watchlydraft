//
//  ImdbMovieObject.m
//  Watchly
//
//  Created by Lambros Tabourlos on 5/18/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import "LCHWatchlyModel.h"
#import "LCHImdbMovieModel.h"

@implementation LCHImdbMovieModel

-(BOOL) validate
{
    if(self == nil) {
        return [super validate];
    }

    if ( [self api_id].length == 0 ) {
        [self setError:@"api_id cannot be empty" with_title:@"api_id error"];
        return NO;
    }
    if ( [self title].length == 0 ) {
        [self setError:@"title cannot be empty" with_title:@"title error"];
        return NO;
    }
    //if ( [self poster].length == 0 ) {
        //[self setError:@"poster cannot be empty" with_title:@"poster error"];
        //return NO;
    //}
    if( [self imdb_id].length > 0 ) {
        NSString *imdb_id_regex = @"tt[0-9]{7}";
        NSPredicate *imdb_id_test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", imdb_id_regex];
    
        return [imdb_id_test evaluateWithObject:self.imdb_id];
    }
    
    return TRUE;
    
}

-(id)initWith_imdb_id:(NSMutableString*)imdb_id title:(NSMutableString*) title poster:(NSMutableString*)poster directors:(NSArray*)directors year:(int)year
{
    self.imdb_id = [ NSMutableString stringWithFormat:@"%@", imdb_id ];
    self.title = [ NSMutableString stringWithFormat:@"%@", title ];
    self.poster = [ NSMutableString stringWithFormat:@"%@", poster ];
    self.directors = [NSMutableArray arrayWithArray:directors];
    self.year = [ NSNumber  numberWithInt:year];
    
    return self;
}
-(id)initWithDictionary:(NSDictionary*) dictionary
{
    self.imdb_id = [dictionary objectForKey:@"id"];
    self.title = [dictionary objectForKey:@"title"];
    self.poster = [dictionary objectForKey:@"poster"];
    self.directors = [NSMutableArray arrayWithArray:[dictionary objectForKey:@"directors"]];
    self.year = [dictionary objectForKey:@"year"];
    
    return self;
}
-(id)initWithMovieDbApiArray:(NSArray*) array
{
    self.api_id = [array valueForKey:@"id"];
    self.title = [array valueForKey:@"title"];
    self.poster = [array valueForKey:@"poster_path"];
    self.year = [array valueForKey:@"release_date"];
    
    return self;
}
-(id)initWithMovieDbApiDictionary:(NSDictionary*) dictionary
{
    //NSString *year = [[dictionary objectForKey:@"release_date"] //substringWithRange:NSMakeRange(0, 4)];
    self.api_id = [dictionary objectForKey:@"id"];
    self.title = [dictionary objectForKey:@"title"];
    self.poster = [dictionary objectForKey:@"poster_path"];
    self.year = [dictionary objectForKey:@"release_date"];//(NSNumber*)year;
    
    return self;
}
-(void)setValuesFromMovieDbApiArray:(NSArray *)array
{
    self.api_id = [array valueForKey:@"id"];
    self.title = [array valueForKey:@"title"];
    self.poster = [array valueForKey:@"poster_path"];
    self.year = [array valueForKey:@"release_date"];
}
-(void)setValuesFromDictionary:(NSDictionary*) dictionary
{
    self.imdb_id = [dictionary objectForKey:@"imdb_id"];
    self.title = [dictionary objectForKey:@"title"];
    self.poster = [dictionary objectForKey:@"poster"];
    self.directors = [NSMutableArray arrayWithArray:[dictionary objectForKey:@"directors"]];
    self.year = [dictionary objectForKey:@"year"];
}
@end
