//
//  WatchlyModel.m
//  Watchly
//
//  Created by Lambros Tabourlos on 5/19/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import "LCHWatchlyModel.h"

@implementation LCHWatchlyModel

-(id)initWithDictionary:(NSDictionary*) dictionary
{
    return self;
}
-(void)parseJson:(NSData *)jsonData
{
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&jsonError];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        self.modelArray = [NSArray arrayWithArray:jsonObject];
    }
    else {
        self.modelArray = [NSArray arrayWithObject:jsonObject];
    }
}
-(void) setError: (NSString*) errorString with_title:(NSString*) title
{
    NSDictionary *userInfoDict = [NSDictionary dictionaryWithObject:errorString forKey:NSLocalizedDescriptionKey];
    NSError *error = [[NSError alloc] initWithDomain:title code:0 userInfo:userInfoDict];
    self.validationError = error;
}
-(BOOL) validate
{
    return NO;
}

@end
