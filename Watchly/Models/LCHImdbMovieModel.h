//
//  ImdbMovieObject.h
//  Watchly
//
//  Created by Lambros Tabourlos on 5/18/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LCHWatchlyModel.h"

@interface LCHImdbMovieModel : LCHWatchlyModel

@property NSMutableString *api_id;
@property NSMutableString *imdb_id;
@property NSMutableString *title;
@property NSMutableString *poster;
@property NSMutableArray *directors;
@property NSNumber* year;

-(id)initWith_imdb_id:(NSString*)imdb_id title:(NSString*) title poster:(NSString*)poster directors:(NSArray*)directors year:(int)year;

-(id)initWithMovieDbApiArray:(NSArray*) array;
-(id)initWithMovieDbApiDictionary:(NSDictionary*) dictionary;
-(void)setValuesFromDictionary:(NSDictionary*) dictionary;
-(void)setValuesFromMovieDbApiArray:(NSArray*) array;

@end
