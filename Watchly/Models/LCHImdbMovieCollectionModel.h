//
//  ImdbMovieCollection.h
//  Watchly
//
//  Created by Lambros Tabourlos on 5/19/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import "LCHWatchlyModel.h"
#import "LCHImdbMovieModel.h"

@interface LCHImdbMovieCollectionModel : LCHWatchlyModel

@property LCHImdbMovieModel* movieObject;
@property NSMutableArray* movieCollection;

-(id) init;

-(void) searchImdbById: (NSString *) imdb_id;
-(void) searchTheMovieDbApiByTitle: (NSString *) title;
-(void) getTopRatedMovies;
-(void) getLatestMovies;

@end
