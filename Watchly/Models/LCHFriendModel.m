//
//  LCHFriendModel.m
//  Watchly
//
//  Created by Lambros Tabourlos on 5/25/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import "LCHFriendModel.h"
#import "LCHAppController.h"

@implementation LCHFriendModel

-(id) initWithUser: (PFUser*) user {
    self.friend = user;
    
    return self;
}

-(void) addFriend {
    NSMutableArray *friendsArray = [ user objectForKey:@"friends"];
    [friendsArray addObject:self.friend];
    [user setObject:[NSArray arrayWithArray:friendsArray] forKey:@"friends"];
    [user saveInBackground];
}

@end
