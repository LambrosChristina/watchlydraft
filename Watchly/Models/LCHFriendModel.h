//
//  LCHFriendModel.h
//  Watchly
//
//  Created by Lambros Tabourlos on 5/25/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import "LCHWatchlyModel.h"

@interface LCHFriendModel : LCHWatchlyModel

@property PFUser *friend;

-(id) initWithUser: (PFUser*) user;
-(void) addFriend;

@end
