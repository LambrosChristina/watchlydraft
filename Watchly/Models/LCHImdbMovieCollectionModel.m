//
//  ImdbMovieCollection.m
//  Watchly
//
//  Created by Lambros Tabourlos on 5/19/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import "LCHImdbMovieCollectionModel.h"

NSString *const topRatedMovies = @"movie/top_rated?api_key=61fb4c4debbda793b81b2c3e9be99b17";
NSString *const latestMovies = @"movie/latest?api_key=61fb4c4debbda793b81b2c3e9be99b17";
NSString *const themoviedbUrl = @"http://api.themoviedb.org/3/";
NSString *const searchMode = @"search/movie?api_key=61fb4c4debbda793b81b2c3e9be99b17&query=";
NSString *const posterUrl = @"http://cf2.imgobject.com/t/p/w500";
NSString *const imdbApiUrl = @"http://imdbapi.org/?type=json&plot=simple&episode=1&limit=10&yg=0&mt=none&lang=en-US&offset=&aka=simple&release=simple&business=0&tech=0";

@implementation LCHImdbMovieCollectionModel

-(id)init
{
    self = [super init];
    self.movieObject = [[LCHImdbMovieModel alloc] init];
    self.movieCollection = [[NSMutableArray alloc] init];
    return self;
}
-(void) searchImdbById: (NSString*) imdb_id
{
    if(imdb_id.length == 0){
        return;
    }
    NSString *url = [NSString stringWithFormat:@"%@&id=%@",imdbApiUrl, imdb_id];
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSURLResponse *resp = nil;
    NSError *error = nil;
    NSData *response = [NSURLConnection sendSynchronousRequest: theRequest returningResponse: &resp error: &error];
    [self parseJson:response];
    
    [self.movieCollection removeAllObjects];
    for (NSDictionary *dictionary in self.modelArray) {
        [self.movieCollection addObject:[[LCHImdbMovieModel alloc] initWithDictionary:dictionary]];
    }
}
-(void) searchTheMovieDbApiByTitle: (NSString *) title;
{
    if(title.length == 0) {
        return;
    }
    NSString *url = [NSString stringWithFormat:@"%@%@%@",themoviedbUrl,searchMode,title];
    url = [ url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSURLResponse *resp = nil;
    NSError *error = nil;
    
    NSData *response = [NSURLConnection sendSynchronousRequest: theRequest returningResponse: &resp error: &error];
    if(response == nil) {
        return;
    }
    [self parseJson:response];
    
    [self.movieCollection removeAllObjects];
    for (NSDictionary *dictionary in self.modelArray ) {
        for(NSDictionary *results in [dictionary objectForKey:@"results"]) {
            [self.movieCollection addObject:[[LCHImdbMovieModel alloc] initWithMovieDbApiDictionary: results]];
        }
    }
}
-(void) getTopRatedMovies
{
    NSString *url = [NSString stringWithFormat:@"%@%@",themoviedbUrl,topRatedMovies];
    url = [ url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSURLResponse *resp = nil;
    NSError *error = nil;
    
    NSData *response = [NSURLConnection sendSynchronousRequest: theRequest returningResponse: &resp error: &error];
    if(response == nil) {
        return;
    }
    [self parseJson:response];
    
    [self.movieCollection removeAllObjects];
    for (NSDictionary *dictionary in self.modelArray ) {
        for(NSDictionary *results in [dictionary objectForKey:@"results"]) {
            [self.movieCollection addObject:[[LCHImdbMovieModel alloc] initWithMovieDbApiDictionary: results]];
        }
    }
}
-(void) getLatestMovies
{
    NSString *url = [NSString stringWithFormat:@"%@%@",themoviedbUrl,latestMovies];
    url = [ url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSURLResponse *resp = nil;
    NSError *error = nil;
    
    NSData *response = [NSURLConnection sendSynchronousRequest: theRequest returningResponse: &resp error: &error];
    if(response == nil) {
        return;
    }
    [self parseJson:response];
    
    [self.movieCollection removeAllObjects];
    for (NSDictionary *dictionary in self.modelArray ) {
        for(NSDictionary *results in [dictionary objectForKey:@"results"]) {
            [self.movieCollection addObject:[[LCHImdbMovieModel alloc] initWithMovieDbApiDictionary: results]];
        }
    }
}

@end
