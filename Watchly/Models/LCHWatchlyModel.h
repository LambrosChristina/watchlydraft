//
//  WatchlyModel.h
//  Watchly
//
//  Created by Lambros Tabourlos on 5/19/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface LCHWatchlyModel : NSObject

@property NSArray *modelArray;
@property NSError *validationError;

-(id)initWithDictionary:(NSDictionary*) dictionary;
-(void)parseJson: (NSData *) jsonData;
-(void)setError: (NSString*) errorString with_title:(NSString*) title;
-(BOOL) validate;

@end
