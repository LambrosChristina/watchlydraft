//
//  likeModel.h
//  Watchly
//
//  Created by Lambros Tabourlos on 5/25/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import "LCHWatchlyModel.h"

@interface LCHLikeModel : LCHWatchlyModel

@property NSMutableString* userId;
@property NSMutableString* firstName;
@property NSMutableString* lastName;

-(id)initWith_userId: (NSString*) userId firstName:(NSString*)firstName lastName:(NSString*) lastName;

@end
