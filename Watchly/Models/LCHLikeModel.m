//
//  LCHLikeModel.m
//  Watchly
//
//  Created by Lambros Tabourlos on 5/25/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import "LCHLikeModel.h"

@implementation LCHLikeModel

-(BOOL)validate
{
    if(self.userId.length == 0) {
        [self setError:@"userId cannot be empty" with_title:@"userId error"];
        return  NO;
    }
    if(self.firstName.length == 0) {
        [self setError:@"firstName cannot be empty" with_title:@"firstName error"];
        return  NO;
    }
    if(self.lastName.length == 0) {
        [self setError:@"lastName cannot be empty" with_title:@"lastName error"];
        return  NO;
    }
    
    return YES;
}
-(id)initWith_userId:(NSString *)userId firstName:(NSString *)firstName lastName:(NSString *)lastName
{
    self.userId = [NSMutableString stringWithFormat:@"%@", userId];
    self.firstName = [NSMutableString stringWithFormat:@"%@", firstName];
    self.lastName = [NSMutableString stringWithFormat:@"%@", lastName];
    
    return self;
}

-(id)initWithDictionary:(NSDictionary *)dictionary
{
    self.userId = [dictionary objectForKey:@"userId"];
    self.firstName = [dictionary objectForKey:@"firstName"];
    self.lastName = [dictionary objectForKey:@"lastName"];
    
    return self;
}


@end
