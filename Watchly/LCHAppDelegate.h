//
//  LCHAppDelegate.h
//  Watchly
//
//  Created by Lambros Tabourlos on 5/18/13.
//  Copyright (c) 2013 Watchly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
